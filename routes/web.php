<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect()->route('home');
    }
    else {
        return view('welcome');
    }
})->name('first');

Auth::routes();

Route::get('info/privacy', 'InfoController@privacy')->name('info.privacy');
Route::get('info/gdxp', 'InfoController@gdxp')->name('info.gdxp');
Route::get('listini/{slug}/{id}', 'SupplierController@landing')->name('supplier.landing');
Route::get('file/{id}/{type}', 'SupplierController@file')->name('supplier.file');

Route::middleware(['auth'])->group(function () {
    Route::get('home', 'HomeController@index')->name('home');

    Route::resource('supplier', 'SupplierController');
    Route::resource('subject', 'SubjectController');
    Route::resource('pact', 'PactController');
    Route::resource('order', 'OrderController');

    Route::get('supplier/{id}/products', 'SupplierController@products')->name('supplier.products');
    Route::post('supplier/{id}/products', 'SupplierController@saveProducts')->name('supplier.products.save');
    Route::post('supplier/{id}/products/import', 'SupplierController@importProducts')->name('supplier.products.import');
    Route::get('supplier/products/import/sample', 'SupplierController@importProductsSample')->name('supplier.products.import.sample');

    Route::put('order/mapzip/read', 'OrderController@mapZip')->name('order.mapzip.read');
    Route::post('order/mapzip/save', 'OrderController@realMapZip')->name('order.mapzip.save');

    Route::get('api/preferred/units', 'InfoController@preferredUnits')->name('preferred.units');
    Route::get('api/preferred/categories', 'InfoController@preferredCategories')->name('preferred.categories');

    Route::middleware(['admin'])->group(function () {
        Route::get('home/update', 'HomeController@update')->name('home.update');
        Route::get('orders/details/{id}', 'OrderController@details')->name('order.details');
        Route::get('orders/details/{id}/{format}', 'OrderController@detailsDownload')->name('order.details.download');
        Route::resource('user', 'UserController');
        Route::resource('client', 'ClientController');
    });
});
