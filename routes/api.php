<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/list', 'ApiController@list')->name('api.list');
Route::get('/get/{vat}', 'ApiController@get')->name('api.get');
Route::get('/pact/{id}', 'ApiController@pact')->name('api.pact');
Route::get('/order/{uuid}', 'ApiController@order')->name('api.order');

Route::post('/push/{token}', 'ApiController@push')->name('api.push');
