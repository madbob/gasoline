<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class OnlyAdmins
{
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::user();
        if (is_null($user) || $user->role != 'admin') {
            abort(401);
        }

        return $next($request);
    }
}
