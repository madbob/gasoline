<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Artisan;
use Session;

use App\Supplier;
use App\Subject;
use App\Pact;
use App\Order;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();

        if ($user->role == 'admin') {
            return view('home');
        }
        else if ($user->role == 'supplier') {
            return view('supplier');
        }
        else if ($user->role == 'des') {
            return view('des');
        }
    }

    public function update()
    {
        Artisan::call('import:es');
        Session::flash('feedback', 'Aggiornamento completato');
        return redirect()->route('home');
    }
}
