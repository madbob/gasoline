<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Hash;

use App\User;

class UserController extends Controller
{
    private function commonSave($user, $request)
    {
        $user->name = $request->input('email');
        $user->email = $request->input('email');
        $user->role = $request->input('role');

        $password = $request->input('password');
        if (filled($password)) {
            $user->password = Hash::make($password);
        }

        if ($user->role == 'supplier') {
            $suppliers = $request->input('suppliers');
        }
        else {
            $suppliers = [];
        }

        if ($user->role == 'des') {
            $pacts = $request->input('pacts');
        }
        else {
            $pacts = [];
        }

        $user->save();
        $user->suppliers()->sync($suppliers);
        $user->pacts()->sync($pacts);
    }

    public function index(Request $request)
    {
        $users = User::orderBy('email', 'asc')->get();
        return view('users', compact('users'));
    }

    public function create()
    {
        return view('user.modal', ['user' => null]);
    }

    public function store(Request $request)
    {
        $user = new User();
        $this->commonSave($user, $request);
        return redirect()->route('user.index');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('user.modal', ['user' => $user]);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $this->commonSave($user, $request);
        return redirect()->route('user.index');
    }
}
