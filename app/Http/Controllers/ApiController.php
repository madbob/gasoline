<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

use App\Helpers\GDXP;
use App\Supplier;
use App\Pact;
use App\Order;
use App\Client;

class ApiController extends Controller
{
    public function list(Request $request)
    {
        return Cache::remember('suppliers_list', 60 * 60 * 24, function() {
            $ret = (object) [
                'count' => 0,
                'results' => [],
            ];

            $suppliers = Supplier::where('active', true)->where('vat', '!=', '')->where('products_file', '!=', '')->get();
            foreach($suppliers as $supplier) {
                $last_update = Carbon::parse(date('Y-m-d', $supplier->gdxpLastUpdate()));
                $ret->count += 1;

                $ret->results[] = (object) [
                    'id' => $supplier->id,
                    'name' => $supplier->name,
                    'vat' => $supplier->vat,
                    'locality' => $supplier->locality,
                    'lastchange' => $last_update->format('Y-m-d'),
                ];
            }

            return response()->json($ret);
        });
    }

    public function get(Request $request, $vat)
    {
        $ret = [];

        $supplier = Supplier::where('vat', $vat)->first();

        if (is_null($supplier)) {
            return '';
        }

        $path = $supplier->gdxpPath();
        if (is_null($path)) {
            return '';
        }

        return response()->json(json_decode(file_get_contents($path)));
    }

    public function pact(Request $request, $id)
    {
        $pact = Pact::find($id);

        $ret = (object) [
            'count' => 0,
            'suppliers' => [],
            'subjects' => [],
        ];

        foreach($pact->suppliers as $supplier) {
            $ret->suppliers[] = (object) [
                'id' => $supplier->id,
                'name' => $supplier->name,
            ];
        }

        foreach($pact->subjects as $subject) {
            $ret->subjects[] = (object) [
                'id' => $subject->id,
                'name' => $subject->name,
            ];
        }

        return response()->json($ret);
    }

    public function order(Request $request, $uuid)
    {
        $order = Order::where('uuid', $uuid)->first();
        if (is_null($order)) {
            return '';
        }

        $ret = GDXP::empty();
        $ret->creationDate = $order->created_at->format('Y-m-d');

        foreach($order->suppliers as $supplier) {
            $gdxp = $supplier->gdxpPath();
            if ($gdxp) {
                $gdxp = json_decode(file_get_contents($gdxp));
                if ($gdxp) {
                    $ret->blocks[] = $gdxp->blocks[0];
                }
                else {
                    Log::error('Invalid GDXP file');
                }
            }
            else {
                Log::error('No GDXP file');
            }
        }

        return response()->json($ret);
    }

    public function push(Request $request, $token)
    {
        $client = Client::where('token', $token)->get();
        if (is_null($client)) {
            abort(403);
        }

        $input_gdxp = json_decode($request->getContent());

        foreach($input_gdxp->blocks as $block) {
            $gdxp = GDXP::empty();
            $gdxp->blocks[] = $block;

            $supplier = Supplier::where('vat', $block->supplier->vatNumber)->first();

            if (is_null($supplier)) {
                $supplier = new Supplier();
                $supplier->name = $block->supplier->name;
                $supplier->vat = $block->supplier->vatNumber;
                $supplier->save();
            }

            $supplier->client_id = $client->id;
            $supplier->save();

            $supplier->saveGdxp($gdxp);
        }

        return response()->json(['status' => 'ok']);
    }
}
