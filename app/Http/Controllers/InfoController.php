<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\GDXP;

class InfoController extends Controller
{
    public function privacy()
    {
        return view('info.privacy');
    }

    public function gdxp()
    {
        return view('info.gdxp');
    }

    private function preferreds($options, $search)
    {
        $ret = (object) [
            'results' => [],
        ];

        foreach($options as $d) {
            $ds = mb_strtolower($d);
            if (strstr($ds, $search) !== false) {
                $ret->results[] = (object) [
                    'id' => $d,
                    'text' => $d,
                ];
            }
        }

        return response()->json($ret);
    }

    public function preferredUnits(Request $request)
    {
        $q = mb_strtolower($request->input('q'));
        $units = GDXP::preferredUnits();
        return $this->preferreds($units, $q);
    }

    public function preferredCategories(Request $request)
    {
        $q = mb_strtolower($request->input('q'));
        $units = GDXP::preferredCategories();
        return $this->preferreds($units, $q);
    }
}
