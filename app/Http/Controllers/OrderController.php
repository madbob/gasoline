<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Log;
use PDF;

use JBZoo\Utils\FS;
use ezcArchive;

use App\Order;
use App\Subject;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Order::class, 'order');
    }

    private function parseDate($date)
    {
        if (strpos($date, '/') !== false) {
            list($d, $m, $y) = explode('/', $date);
            return sprintf('%s-%s-%s', $y, $m, $d);
        }
        else {
            return $date;
        }
    }

    private function commonSave($order, $request)
    {
        if ($order->exists == false) {
            $order->uuid = (string) Str::uuid();
        }

        $order->start = $this->parseDate($request->input('start'));
        $order->end = $this->parseDate($request->input('end'));
        $order->save();

        $order->pacts()->sync($request->input('pacts', []));

        $suppliers = $request->input('suppliers', []);
        $actual_suppliers = [];

        foreach($suppliers as $supplier_id) {
            if (!filled($supplier_id)) {
                continue;
            }

            $path = '';
            $key = 'products_file_' . $supplier_id;

            if ($request->hasFile($key)) {
                $path = $request->$key->store('gdxp');
            }
            else {
                $existing = $order->suppliers()->where('suppliers.id', $supplier_id)->first();
                if ($existing) {
                    $path = $existing->pivot->products_file;
                }
            }

            $actual_suppliers[$supplier_id] = ['products_file' => $path];
        }

        $order->suppliers()->sync($actual_suppliers);

        $subjects = $request->input('subjects', []);
        $actual_subjects = [];

        foreach($subjects as $subject_id) {
            if (!filled($subject_id)) {
                continue;
            }

            $path = '';
            $key = 'booking_file_' . $subject_id;

            if ($request->hasFile($key)) {
                $path = $request->$key->store('gdxp');
            }
            else {
                $existing = $order->subjects()->where('subjects.id', $subject_id)->first();
                if ($existing) {
                    $path = $existing->pivot->booking_file;
                }
            }

            $actual_subjects[$subject_id] = ['booking_file' => $path];
        }

        $order->subjects()->syncWithoutDetaching($actual_subjects);
    }

    public function readSubjectsZip($filepath)
    {
        $archive = ezcArchive::open($filepath);
        $temp_folder = sprintf('%s/extracted_zip_%s', sys_get_temp_dir(), Str::random(10));
        mkdir($temp_folder, 0777);

        $data = (object) [
            'zip_path' => $filepath,
            'path' => $temp_folder,
            'contents' => [],
        ];

        while ($archive->valid()) {
            $entry = $archive->current();
            $archive->extractCurrent($temp_folder);
            $temp_path = $temp_folder . '/' . $entry->getPath();

            $contents = json_decode(file_get_contents($temp_path));
            $data->contents[] = (object) [
                'name' => $contents->subject->name,
                'file' => $entry->getPath(),
            ];

            $archive->next();
        }

        return $data;
    }

    public function mapZip(Request $request)
    {
        $id = $request->input('order_id');
        $order = Order::find($id);
        $path = $request->booking_file_multiple->store('temp');
        $info = $this->readSubjectsZip(storage_path('app/' . $path));
        return view('order.mapzip', ['info' => $info, 'order' => $order, 'path' => $path]);
    }

    public function realMapZip(Request $request)
    {
        $id = $request->input('order_id');
        $order = Order::find($id);

        $original_path = $request->input('folder_path');
        $info = $this->readSubjectsZip($original_path);

        $subjects = $request->input('subjects');
        $original_filenames = $request->input('original_filename');
        $actual_subjects = [];

        foreach($subjects as $index => $subject) {
            $filename = $original_filenames[$index];
            $old_path = $info->path . '/' . $filename;

            if ($subject == 'new') {
                \Log::debug('creo');
                $s = new Subject();
                $contents = json_decode(file_get_contents($old_path));
                $s->name = $contents->subject->name;
                $s->key = (string) Str::uuid();
                $s->save();

                $subject = $s->id;
            }
            else if ($subject == 0) {
                \Log::debug('salto');
                continue;
            }

            do {
                $final_filename = Str::random(20);
                $path = storage_path('app/gdxp/' . $final_filename);
            } while(file_exists($path));

            rename($old_path, $path);
            $actual_subjects[$subject] = ['booking_file' => 'gdxp/' . $final_filename];
        }

        $order->subjects()->syncWithoutDetaching($actual_subjects);
        @unlink($original_path);
        return redirect()->route('home');
    }

    public function create()
    {
        return view('order.modal', ['order' => null]);
    }

    public function store(Request $request)
    {
        $order = new Order();
        $this->commonSave($order, $request);
        return redirect()->route('home');
    }

    public function edit(Order $order)
    {
        foreach($order->pacts as $pact) {
            foreach($pact->subjects as $subject) {
                $existing = $order->subjects()->where('subjects.id', $subject->id)->first();
                if (is_null($existing)) {
                    $order->subjects()->attach($subject->id);
                }
            }
        }

        return view('order.modal', ['order' => $order]);
    }

    public function update(Request $request, Order $order)
    {
        $this->commonSave($order, $request);
        return redirect()->route('home');
    }

    public function details($id)
    {
        $order = Order::find($id);
        return view('order.details', ['order' => $order]);
    }

    public function detailsDownload($id, $format)
    {
        $order = Order::find($id);
        $data = $order->aggregate();

        switch($format) {
            case 'pdf':
                $contents = [];
                $first_column_done = false;

                foreach($data as $supplier) {
                    foreach($supplier->products as $product) {
                        foreach($product->subjects as $index => $subject) {
                            if ($first_column_done == false) {
                                $contents[$index] = (object) [
                                    'name' => $subject->name,
                                    'products' => [],
                                ];
                            }

                            if ($subject->quantity != 0) {
                                $contents[$index]->products[] = (object) [
                                    'name' => $product->name,
                                    'quantity' => $subject->quantity,
                                ];
                            }
                        }

                        $first_column_done = true;
                    }
                }

                $last_row = (object) [
                    'name' => 'Totali'
                ];

                foreach($data as $supplier) {
                    foreach($supplier->products as $product) {
                        $last_row->products[] = (object) [
                            'name' => $product->name,
                            'quantity' => $product->quantity,
                        ];
                    }
                }

                $contents[] = $last_row;

                $pdf = PDF::loadView('order.details_pdf', ['contents' => $contents]);
                return $pdf->download('aggregato.pdf');
                break;

            case 'csv':
                $headers = ['GAS'];

                foreach($data as $supplier) {
                    foreach($supplier->products as $product) {
                        $headers[] = $product->name;
                    }
                }

                $contents = [];
                $first_column_done = false;

                foreach($data as $supplier) {
                    foreach($supplier->products as $product) {
                        foreach($product->subjects as $index => $subject) {
                            if ($first_column_done == false) {
                                $contents[$index] = [$subject->name];
                            }

                            $contents[$index][] = $subject->quantity;
                        }

                        $first_column_done = true;
                    }
                }

                $last_row = ['Totali'];

                foreach($data as $supplier) {
                    foreach($supplier->products as $product) {
                        $last_row[] = $product->quantity;
                    }
                }

                $contents[] = $last_row;

                return output_csv('aggregato.csv', $headers, $contents, null);
                break;
        }
    }
}
