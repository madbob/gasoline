<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Pact;

class PactController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Pact::class, 'pact');
    }

    private function commonSave($pact, $request)
    {
        $pact->name = $request->input('name');

        if ($pact->exists == false) {
            $pact->uuid = (string) Str::uuid();
        }

        $pact->save();

        $pact->suppliers()->sync($request->input('suppliers', []));
        $pact->subjects()->sync($request->input('subjects', []));
    }

    public function edit(Pact $pact)
    {
        return view('pact.modal', ['pact' => $pact]);
    }

    public function update(Request $request, Pact $pact)
    {
        $this->commonSave($pact, $request);
        return redirect()->route('home');
    }
}
