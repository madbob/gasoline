<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Subject;

class SubjectController extends Controller
{
    public function edit($id)
    {
        $subject = Subject::find($id);
        return view('subject.modal', ['subject' => $subject]);
    }

    public function update(Request $request, $id)
    {
        $subject = Subject::find($id);
        $subject->name = $request->input('name');
        $subject->parent_id = $request->input('parent_id');
        $subject->save();
        return redirect()->route('home');
    }
}
