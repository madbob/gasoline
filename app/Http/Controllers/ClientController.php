<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Client;

class ClientController extends Controller
{
    private function commonSave($client, $request)
    {
        $client->name = $request->input('name');

        if (empty($client->token)) {
            $client->token = Str::random(10);
        }

        $client->save();
    }

    public function index(Request $request)
    {
        $clients = Client::orderBy('created_at', 'desc')->get();
        return view('clients', compact('clients'));
    }

    public function create()
    {
        return view('client.modal', ['client' => null]);
    }

    public function store(Request $request)
    {
        $client = new Client();
        $this->commonSave($client, $request);
        return redirect()->route('client.index');
    }

    public function edit($id)
    {
        $client = Client::find($id);
        return view('client.modal', ['client' => $client]);
    }

    public function update(Request $request, $id)
    {
        $client = Client::find($id);
        $this->commonSave($client, $request);
        return redirect()->route('client.index');
    }
}
