<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Log;
use Cache;
use Session;
use Response;

use App\Helpers\GDXP;
use App\Supplier;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Supplier::class, 'supplier');
    }

    private function commonSave($supplier, $request)
    {
        $supplier->name = $request->input('name');
        $supplier->vat = $request->input('vat');
        $supplier->active = $request->has('active');

        if ($request->hasFile('products_file')) {
            $supplier->products_file = $request->products_file->store('gdxp');
            $path = $supplier->gdxpPath();
            if (GDXP::test(json_decode(file_get_contents($path))) == false) {
                $supplier->products_file = '';
                Log::debug('File GDXP allegato al fornitore non valido');
                Session::flash('feedback', 'File GDXP non valido!');
            }
            else {
                Cache::forget('suppliers_list');
            }
        }
        else {
            if ($request->has('delete_file')) {
                $path = $supplier->gdxpPath();
                if ($path) {
                    unlink($path);
                }

                $supplier->products_file = '';
            }
        }

        $supplier->save();
    }

    public function edit(Request $request, Supplier $supplier)
    {
        return view('supplier.modal', ['supplier' => $supplier]);
    }

    public function update(Request $request, Supplier $supplier)
    {
        $this->commonSave($supplier, $request);
        return redirect()->route('home');
    }

    public function file(Request $request, $id, $type)
    {
        $supplier = Supplier::find($id);
        if (is_null($supplier)) {
            abort(404);
        }

        $path = $supplier->gdxpPath();
        if (is_null($path)) {
            abort(404);
        }

        if ($type == 'gdxp') {
            return response()->download($path);
        }
        else if ($type == 'csv') {
            return response()->download(GDXP::toCsv(file_get_contents($path)))->deleteFileAfterSend();
        }
        else {
            abort(404);
        }
    }

    public function products(Request $request, $id)
    {
        $user = $request->user();
        $supplier = Supplier::find($id);

        if ($user->can('update', $supplier)) {
            return view('supplier.products', ['supplier' => $supplier]);
        }
        else {
            abort(401);
        }
    }

    public function saveProducts(Request $request, $id)
    {
        $user = $request->user();
        $supplier = Supplier::find($id);

        if ($user->can('update', $supplier)) {
            $gdxp = GDXP::empty();
            $gdxp->blocks[] = json_decode($request->input('gdxp'));
            $supplier->saveGdxp($gdxp);
            Cache::forget('suppliers_list');
            Session::flash('saved', 'true');
        }
        else {
            abort(401);
        }
    }

    public function importProductsSample(Request $request)
    {
        $callback = function() {
            $file = fopen('php://output', 'w');
            $cols = array_keys(GDXP::csvColumns());
            fputcsv($file, $cols);
            fclose($file);
        };

        return Response::stream($callback, 200, [
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=listino_prodotti.csv',
            'Expires' => '0',
            'Pragma' => 'public'
        ]);
    }

    private function parseDecimal($value, $default = 0)
    {
        $value = trim($value);

        if (filled($value) == false) {
            return $default;
        }

        return strtr($value, ',', '.');
    }

    private function guessCsvFileSeparator($path)
    {
        $contents = fopen($path, 'r');
        if ($contents === false) {
            return null;
        }

        $separators = [',', ';', "\t"];
        $lenghts = [0, 0, 0];

        foreach($separators as $sep_index => $sep) {
            $row = fgetcsv($contents, 0, $sep);
            $lenghts[$sep_index] = count($row);
            rewind($contents);
        }

        $target_separator = $separators[array_search(max($lenghts), $lenghts)] ?? null;
        if (is_null($target_separator)) {
            throw new \Exception(_i('Impossibile interpretare il file'), 1);
        }

        return $target_separator;
    }

    public function importProducts(Request $request, $id)
    {
        $user = $request->user();
        $supplier = Supplier::find($id);

        if ($user->can('update', $supplier)) {
            $data = $supplier->gdxpPath();
            if ($data) {
                $data = json_decode(file_get_contents($data));
            }
            else {
                $data = GDXP::empty();
                $data->blocks[] = (object) [
                    'supplier' => (object) [
                        'products' => [],
                    ],
                ];
            }

            $csv_path = $request->csv->store('temp');
            $csv_path = storage_path('app/' . $csv_path);

            $separator = $this->guessCsvFileSeparator($csv_path);

            $file = fopen($csv_path, 'r');
            fgetcsv($file, null, $separator);

            while($row = fgetcsv($file, null, $separator)) {
                $new = (object) [
                    'name' => $row[0],
                    'um' => $row[2],
                    'category' => $row[3],
                    'sku' => $row[4],
                    'description' => $row[5],
                    'orderInfo' => (object) [
                        'packageQty' => $this->parseDecimal($row[6], 1),
                        'umPrice' => $this->parseDecimal($row[7]),
                        'vatRate' => $this->parseDecimal($row[8], 22),
                    ],
                    'active' => boolval($row[1]),
                ];

                $found = false;

                foreach($data->blocks[0]->supplier->products as $index => $product) {
                    if ($product->sku == $new->sku) {
                        array_splice($data->blocks[0]->supplier->products, $index, 1, [$new]);
                        $found = true;
                        break;
                    }
                }

                if ($found == false) {
                    $data->blocks[0]->supplier->products[] = $new;
                }
            }

            unlink($csv_path);

            $supplier->saveGdxp($data);
            Cache::forget('suppliers_list');
            Session::flash('saved', 'true');

            return redirect()->route('supplier.products', $supplier->id);
        }
        else {
            abort(401);
        }
    }

    public function landing(Request $request, $slug, $id)
    {
        $supplier = Supplier::find($id);
        if (is_null($supplier)) {
            return redirect()->route('home');
        }

        return view('supplier.landing', ['supplier' => $supplier]);
    }
}
