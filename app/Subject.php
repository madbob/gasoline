<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public function children()
    {
        return $this->hasMany('App\Subject', 'parent_id');
    }

    /*
        Questo va invocato solo sui soggetti correlati ad un Order (e.g. $order->subjects)
    */
    public function gdxpLastUpdate()
    {
        if (filled($this->pivot->booking_file)) {
            $path = storage_path('app/' . $this->pivot->booking_file);

            if ($path) {
                $contents = json_decode(file_get_contents($path));
                if (isset($contents->creationDate) && preg_match('/[0-9]{4,4}-[0-9]{1,2}-[0-9]{1,2}/', $contents->creationDate)) {
                    return strtotime($contents->creationDate);
                }
                else {
                    return filemtime($path);
                }
            }
        }

        return null;
    }
}
