<?php

namespace App\Helpers;

use Opis\JsonSchema\Validator;

class GDXP
{
    public static function empty()
    {
        return (object) [
            'protocolVersion' => '1.0',
            'creationDate' => date('Y-m-d'),
            'applicationSignature' => 'Hub',
            'subject' => (object) [
                'name' => '',
                'taxCode' => '',
                'vatNumber' => '',
                'address' => (object) [
                    'street' => '',
                    'locality' => '',
                    'zipCode' => ''
                ],
                'contacts' => []
            ],
            'blocks' => []
        ];
    }

    public static function test($gdxp)
    {
        $schema = file_get_contents(config_path('GDXP-1.json'));
        $validator = new Validator();
        $result = $validator->dataValidation($gdxp, $schema);
        $test = $result->isValid();

        if ($result->hasErrors()) {
            \Log::debug('Fallita validazione GDXP: ');
            foreach($result->getErrors() as $error) {
                \Log::debug("\t" . $error->error());
                \Log::debug("\t\t" . join(', ', $error->dataPointer()));
            }
        }

        return $test;
    }

    public static function preferredUnits()
    {
        return [
            'Bag-in-Box',
            'Barattoli',
            'Bottiglie',
            'Buste',
            'Cassette',
            'Chili',
            'Flaconi',
            'Grammi',
            'Latte/Lattine',
            'Mazzi',
            'Mezze cassette',
            'Non Specificato',
            'Pacchetti',
            'Pacchi',
            'Pagnotte',
            'Pezzi',
            'Sacchetti',
            'Sacchi',
            'Scatole',
            'Teste',
            'Tubi',
            'Vaschette',
            'Vassoi',
        ];
    }

    public static function preferredCategories()
    {
        return [
            'Abbigliamento',
            'Aceto',
            'Altri alimentari',
            'Altri non alimentari',
            'Bevande',
            'Bevande analcoliche',
            'Biancheria',
            'Birra',
            'Biscotti e dolci',
            'Caffè, the e tisane',
            'Calzature',
            'Carne e affini',
            'Carni bianche',
            'Carni di bovino',
            'Carni di suino',
            'Cereali e derivati',
            'Cereali per colazione',
            'Cereali secchi',
            'Cioccolato, cacao',
            'Composte di verdura',
            'Cosmetici',
            'Cura della persona',
            'Formaggi di latte vaccino',
            'Frutta e verdura',
            'Frutta fresca',
            'Frutta secca',
            'Latte e latticini',
            'Legumi secchi',
            'Marmellate e composte di frutta',
            'Miele, zucchero, dolcificanti',
            'Non Specificato',
            'Olio di oliva, olive e derivati',
            'Pane e farine di altri cereali',
            'Pane e farine di grano',
            'Parmigiano',
            'Pasta fresca e ripiena',
            'Pasta secca',
            'Pecorini e caprini',
            'Pesce congelato',
            'Pesce conservato',
            'Pesce e derivati',
            'Pesce fresco',
            'Riso e derivati',
            'Salse e sughi',
            'Salumi e insaccati',
            'Saponi e detersivi',
            'Spezie e piante aromatiche',
            'Succhi di frutta e di verdura',
            'Sughi di carne',
            'Tessuti e calzature',
            'Uova',
            'Verdura fresca',
            'Vini bianchi e rossi',
            'Yoghurt',
        ];
    }

    public static function csvColumns()
    {
        return [
            'Nome' => [
                'explain' => "Nome del prodotto.",
            ],
            'Ordinabile' => [
                'explain' => "1 se il prodotto è acquistabile, 0 altrimenti.",
            ],
            'Unità di Misura' => [
                'explain' => "Questo non fa riferimento alla taglia del prodotto bensì all'unità acquistabile non frazionabile, dunque per un pacco da 500g si dovrà indicare qui 'Pacco' e non '500g' (500g potrà essere indicato ad esempio nel nome). Si consiglia si usarne una tra quelle consigliate."
            ],
            'Categoria' => [
                'explain' => "Macro gruppo del prodotto. Si consiglia si usarne una tra quelle consigliate.",
            ],
            'Codice Fornitore' => [
                'explain' => "Codice che identifica univocamente il prodotto, anche se il nome dovesse cambiare in futuro."
            ],
            'Descrizione' => [
                'explain' => "Testo descrittivo del prodotto. Qui possono essere indicate le informazioni altrove non codificate.",
            ],
            'Dimensione Confezione' => [
                'explain' => "In caso di prodotti acquistabili da parte del GAS solo per multipli di una certa quantità, indicare qui tale quantità. Questo attributo non fa riferimento al numero di pezzi inclusi in una confezione acquistabile atomicamente e non frazionabile.",
            ],
            'Prezzo Unitario' => [
                'explain' => "Prezzo unitario del prodotto, in Euro. Deve essere comprensivo di IVA.",
            ],
            'Aliquota IVA' => [
                'explain' => "Aliquota IVA del prodotto. E.g. 22 (inteso come 22%).",
            ],
            'Totale Disponibile' => [
                'explain' => "Se è disponibile solo una certa quantità complessiva di prodotto, va indicato qui. Lasciare vuoto (o 0) altrimenti.",
            ],
        ];
    }

    public function toCsv($gdxp)
    {
        $filepath = tempnam(sys_get_temp_dir(), 'csv_');
        $row = array_keys(self::csvColumns());

        $file = fopen($filepath, 'w+');
        fputcsv($file, $row);

        foreach($gdxp->blocks as $block) {
            foreach($block->products as $product) {
                $row = [
                    $product->name,
                    $product->active ? '1' : '0',
                    $product->um,
                    $product->category,
                    $product->sku,
                    $product->description,
                    $product->orderInfo->packageQty,
                    $product->orderInfo->umPrice,
                    $product->orderInfo->vatRate,
                    $product->orderInfo->availableQty,
                ];

                fputcsv($file, $row);
            }
        }

        fclose($file);
        return $filepath;
    }
}
