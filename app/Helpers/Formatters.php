<?php

function output_csv($filename, $head, $contents, $format_callback, $out_file = null)
{
    $callback = function() use ($head, $contents, $format_callback, $out_file) {
        if (is_null($out_file))
            $FH = fopen('php://output', 'w');
        else
            $FH = fopen($out_file, 'w');

        if (is_null($format_callback)) {
            if ($head) {
                fputcsv($FH, $head);
            }

            if (is_string($contents)) {
                fwrite($FH, $contents);
            }
            else if (is_array($contents)) {
                foreach ($contents as $c) {
                    fputcsv($FH, $c);
                }
            }
        }
        else {
            if ($head) {
                fputcsv($FH, $head);
            }

            foreach ($contents as $c) {
                $row = $format_callback($c);
                if ($row) {
                    fputcsv($FH, $row);
                }
            }
        }

        fclose($FH);
    };

    if (is_null($out_file)) {
        $headers = [
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename=' . str_replace(' ', '\\', $filename),
            'Expires' => '0',
            'Pragma' => 'public'
        ];

        return Response::stream($callback, 200, $headers);
    }
    else {
        $callback();
        return $out_file;
    }
}
