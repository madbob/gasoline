<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use Log;

use App\Order;
use App\User;

class OrderPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Order $order)
    {
        if ($user->role == 'admin') {
            return true;
        }
        else if ($user->role == 'des') {
            $enabled_pacts = $user->pacts()->pluck('id')->toArray();

            foreach($order->pacts as $pact) {
                if (in_array($pact->id, $enabled_pacts)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function create(User $user)
    {
        return $user->role == 'admin' || $user->role == 'des';
    }

    public function update(User $user, Order $order)
    {
        return $this->view($user, $order);
    }

    public function delete(User $user, Order $order)
    {
        return $this->view($user, $order);
    }
}
