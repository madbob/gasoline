<?php

namespace App\Policies;

use App\Pact;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PactPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Pact $pact)
    {
        return $user->role == 'admin' || ($user->role == 'des' && $user->pacts()->where('pact_id', $pact->id)->count() != 0);
    }

    public function create(User $user)
    {
        return $user->role == 'admin' || $user->role == 'des';
    }

    public function update(User $user, Pact $pact)
    {
        return $user->role == 'admin' || ($user->role == 'des' && $user->pacts()->where('pact_id', $pact->id)->count() != 0);
    }

    public function delete(User $user, Pact $pact)
    {
        return $user->role == 'admin';
    }
}
