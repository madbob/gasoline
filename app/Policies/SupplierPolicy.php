<?php

namespace App\Policies;

use App\Supplier;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SupplierPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Supplier $supplier)
    {
        return $user->role == 'admin' || $user->suppliers()->where('supplier_id', $supplier->id)->count() != 0;
    }

    public function update(User $user, Supplier $supplier)
    {
        return $user->role == 'admin' || $user->suppliers()->where('supplier_id', $supplier->id)->count() != 0;
    }

    public function delete(User $user, Supplier $supplier)
    {
        return $user->role == 'admin';
    }
}
