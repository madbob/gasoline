<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;

class Supplier extends Model
{
    public function gdxpPath()
    {
        /*
            Se sto accedendo al file GDXP di un fornitore correlato ad un
            ordine, do precedenza al listino caricato in tale contesto
        */
        if (isset($this->pivot->products_file) && filled($this->pivot->products_file)) {
            $attribute = $this->pivot->products_file;
        }
        else {
            $attribute = $this->products_file;
        }

        if (filled($attribute)) {
            $path = storage_path('app/' . $attribute);
            if (file_exists($path)) {
                return $path;
            }
        }

        return null;
    }

    public function getLocalityAttribute()
    {
        $path = $this->gdxpPath();
        if ($path) {
            $contents = json_decode(file_get_contents($path));
            return $contents->blocks[0]->supplier->address->locality ?? '';
        }
        else {
            return '';
        }
    }

    public function gdxpLastUpdate()
    {
        $path = $this->gdxpPath();
        if ($path) {
            $contents = json_decode(file_get_contents($path));
            if (isset($contents->creationDate) && preg_match('/[0-9]{4,4}-[0-9]{1,2}-[0-9]{1,2}/', $contents->creationDate)) {
                return strtotime($contents->creationDate);
            }
            else {
                return filemtime($path);
            }
        }
        else {
            return null;
        }
    }

    public function gdxpLastUpdateHuman()
    {
        $last = $this->gdxpLastUpdate();
        if ($last) {
            return date('d/m/Y', $last);
        }
        else {
            return 'Mai';
        }
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function pacts()
    {
        return $this->belongsToMany('App\Pact');
    }

    public static function getRand()
    {
        $test = 0;

        while($test < 10) {
            $supplier = self::where('products_file', '!=', '')->inRandomOrder()->first();
            if ($supplier && $supplier->gdxpPath()) {
                break;
            }

            $test++;
            $supplier = null;
        }

        return $supplier;
    }

    public function getLandingUrl()
    {
        return route('supplier.landing', ['slug' => Str::slug($this->name), 'id' => $this->id]);
    }

    public function saveGdxp($gdxp)
    {
        $path = 'gdxp/' . Str::random(40) . '.txt';
        $fullpath = storage_path('app/' . $path);
        file_put_contents($fullpath, json_encode($gdxp));
        $this->products_file = $path;
        $this->save();
    }

    /*
        Funzione di comodo per appiattire i contenuti formattati nel GDXP in
        oggetti più facilmente gestibili nel form di edit del listino.
        Non salvare un fornitore dopo aver eseguito questa funzione.
    */
    public function fromGdxp($data)
    {
        if ($data) {
            $this->taxCode = $data->blocks[0]->supplier->taxCode ?? '';
            $this->vatNumber = $data->blocks[0]->supplier->vatNumber ?? '';
            $this->contacts = $data->blocks[0]->supplier->contacts ?? [];
            $this->street = $data->blocks[0]->supplier->address->street ?? '';
            $this->locality = $data->blocks[0]->supplier->address->locality ?? '';
            $this->zipCode = $data->blocks[0]->supplier->address->zipCode ?? '';

            $products = [];

            foreach($data->blocks[0]->supplier->products as $product) {
                $product->um = $product->um ?? '';
                $product->category = $product->category ?? '';
                $product->packageQty = $product->orderInfo->packageQty ?? '';
                $product->umPrice = $product->orderInfo->umPrice ?? '';
                $product->maxQty = $product->orderInfo->maxQty ?? '';
                $product->minQty = $product->orderInfo->minQty ?? '';
                $product->mulQty = $product->orderInfo->mulQty ?? '';
                $product->availableQty = $product->orderInfo->availableQty ?? '';
                $product->vatRate = $product->orderInfo->vatRate ?? '';

                $products[] = $product;
            }

            $this->products = $products;
        }
        else {
            $this->products = [];
        }
    }
}
