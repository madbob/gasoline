<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function suppliers()
    {
        return $this->belongsToMany('App\Supplier');
    }

    public function pacts()
    {
        return $this->belongsToMany('App\Pact');
    }

    public function accessibleSuppliers()
    {
        if ($this->role == 'admin') {
            return Supplier::orderBy('name', 'asc')->get();
        }
        else if ($this->role == 'des') {
            $enabled_pacts = $this->pacts->pluck('id');

            return Supplier::whereHas('pacts', function($query) use ($enabled_pacts) {
                $query->whereIn('pact_id', $enabled_pacts);
            })->orderBy('created_at', 'desc')->get();
        }
        else {
            return $this->suppliers;
        }
    }

    public function accessibleSubjects()
    {
        if ($this->role == 'admin') {
            return Subject::orderBy('name', 'asc')->get();
        }
        else if ($this->role == 'des') {
            $enabled_pacts = $user->pacts->pluck('id');

            return Subject::whereHas('pacts', function($query) use ($enabled_pacts) {
                $query->whereIn('pact_id', $enabled_pacts);
            })->orderBy('created_at', 'desc')->get();
        }
        else {
            return new Collection();
        }
    }

    public function accessiblePacts()
    {
        if ($this->role == 'admin') {
            return Pact::orderBy('name', 'asc')->get();
        }
        else {
            return $this->pacts;
        }
    }

    public function accessibleOrders()
    {
        if ($this->role == 'admin') {
            return Order::orderBy('created_at', 'desc')->get();
        }
        else {
            $enabled_pacts = $this->pacts->pluck('id');

            return Order::whereHas('pacts', function($query) use ($enabled_pacts) {
                $query->whereIn('pact_id', $enabled_pacts);
            })->orderBy('created_at', 'desc')->get();
        }
    }
}
