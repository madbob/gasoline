<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pact extends Model
{
    public function suppliers()
    {
        return $this->belongsToMany('App\Supplier');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Subject');
    }
}
