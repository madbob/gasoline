<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

use App\Supplier;

class RebuildGDXPRelations extends Command
{
    protected $signature = 'import:files';
    protected $description = 'Rilegge tutti i files GDXP esistenti ed aggiorna le relazioni sul DB';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $folder = storage_path('app/gdxp');
        $files = scandir($folder);

        foreach($files as $file) {
            if (Str::endsWith($file, 'json') || Str::endsWith($file, 'txt')) {
                $filepath = storage_path('app/gdxp/' . $file);
                $contents = json_decode(file_get_contents($filepath));
                if ($contents) {
                    if (count($contents->blocks) != 1) {
                        continue;
                    }

                    foreach($contents->blocks as $block) {
                        $taxcode = $block->supplier->taxCode;
                        $supplier = Supplier::where('vat', $taxcode)->first();
                        if ($supplier && empty($supplier->products_file)) {
                            $this->info($supplier->name . ' -> ' . $file);
                            $supplier->products_file = 'gdxp/' . $file;
                            $supplier->save();
                        }
                    }
                }
            }
        }
    }
}
