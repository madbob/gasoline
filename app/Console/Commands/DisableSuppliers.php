<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

use App\Supplier;

class DisableSuppliers extends Command
{
    protected $signature = 'disable:suppliers';
    protected $description = 'Disabilita i fornitori con listini più vecchi di un anno';

    public function handle()
    {
        $one_year_ago = Carbon::today()->subYears(1);
        $suppliers = Supplier::where('active', true)->where('products_file', '!=', '')->get();

        foreach($suppliers as $supplier) {
            $last_update = Carbon::parse(date('Y-m-d', $supplier->gdxpLastUpdate()));
            if ($last_update->lessThan($one_year_ago)) {
                $supplier->active = false;
                $supplier->save();
            }
        }
    }
}
