<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

use Log;

use App\Supplier;
use App\Subject;
use App\Pact;

class ImportEconomiaSolidale extends Command
{
    protected $signature = 'import:es';
    protected $description = 'Per importare i contenuti da Economia Solidale';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        /*
            FORNITORI
        */

        $saved_suppliers = [];
        $suppliers = json_decode(file_get_contents('http://www.economiasolidale.net/api/suppliers'));
        foreach($suppliers as $data) {
            $s = Supplier::where('external_id', $data->nid)->first();
            if (is_null($s)) {
                $s = new Supplier();
            }

            $s->name = $data->title;
            $s->vat = $data->field_chi_piva;
            $s->external_id = $data->nid;
            $s->save();

            $saved_suppliers[] = $s->id;
        }

        Supplier::whereNotIn('id', $saved_suppliers)->where('client_id', 0)->doesntHave('users')->delete();

        /*
            SOGGETTI
        */

        $saved_subjects = [];
        $subjects = json_decode(file_get_contents('http://www.economiasolidale.net/api/subjects'));
        foreach($subjects as $data) {
            $s = Subject::where('external_id', $data->nid)->first();
            if (is_null($s)) {
                $s = new Subject();
                $s->key = (string) Str::uuid();
            }

            $s->name = $data->title;
            $s->external_id = $data->nid;
            $s->save();

            $saved_subjects[] = $s->id;
        }

        Subject::whereNotIn('id', $saved_subjects)->where('external_id', '!=', 0)->delete();

        /*
            PATTI
        */

        $pacts = [];
        $subjects_in_pacts = json_decode(file_get_contents('http://www.economiasolidale.net/api/pacts'));

        foreach($subjects_in_pacts as $data) {
            $pact = $data->field_chi_patto;
            if (!isset($pacts[$pact])) {
                $pacts[$pact] = (object) [
                    'suppliers' => [],
                    'subjects' => [],
                ];
            }

            $target = Supplier::where('external_id', $data->nid)->first();
            if (is_null($target)) {
                $target = Subject::where('external_id', $data->nid)->first();
                if (is_null($target)) {
                    Log::error('Impossibile assegnare nodo a patto: ' . $data->nid);
                }
                else {
                    $pacts[$pact]->subjects[] = $target->id;
                }
            }
            else {
                $pacts[$pact]->suppliers[] = $target->id;
            }
        }

        foreach($pacts as $pact_name => $subjects) {
            $pact = Pact::where('name', $pact_name)->first();
            if (is_null($pact)) {
                $pact = new Pact();
                $pact->uuid = (string) Str::uuid();
                $pact->name = $pact_name;
                $pact->save();
            }

            $pact->suppliers()->sync($subjects->suppliers);
            $pact->subjects()->sync($subjects->subjects);
        }
    }
}
