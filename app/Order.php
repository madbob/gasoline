<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;
use Illuminate\Support\Collection;

class Order extends Model
{
    public function pacts()
    {
        return $this->belongsToMany('App\Pact');
    }

    public function suppliers()
    {
        return $this->belongsToMany('App\Supplier')->withPivot(['products_file']);
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Subject')->withPivot(['booking_file']);
    }

    public function humanDate($name)
    {
        list($y, $m, $d) = explode('-', $this->$name);
        return sprintf('%d/%d/%d', $d, $m, $y);
    }

    private function productIdentifier($product)
    {
        if (isset($product->global_id) && filled($product->global_id)) {
            return $product->global_id;
        }
        else if (isset($product->sku) && filled($product->sku)) {
            return $product->sku;
        }
        else {
            return Str::slug($product->name);
        }
    }

    private function supplierIdentifier($supplier)
    {
        return $supplier->vatNumber ?? ($supplier->vat ?? Str::slug($supplier->name));
    }

    public function aggregate()
    {
        $data = [];

        foreach ($this->suppliers as $supplier) {
            $gdxp_path = $supplier->gdxpPath();
            if (filled($gdxp_path)) {
                $supplier_identifier = $this->supplierIdentifier($supplier);
                $gdxp = json_decode(file_get_contents($gdxp_path));

                $s = (object) [
                    'identifier' => $supplier_identifier,
                    'name' => $supplier->name,
                    'products' => [],
                ];

                foreach($gdxp->blocks as $block) {
                    foreach($block->supplier->products as $product) {
                        $identifier = $this->productIdentifier($product);

                        $p = (object) [
                            'name' => $product->name,
                            'uuid' => $identifier,
                            'quantity' => 0,
                            'subjects' => [],
                        ];

                        $s->products[$identifier] = $p;
                    }
                }

                $data[$supplier_identifier] = $s;
            }
        }

        foreach ($this->subjects as $subject) {
            if (filled($subject->pivot->booking_file)) {
                $gdxp = json_decode(file_get_contents(storage_path('app/' . $subject->pivot->booking_file)));

                foreach($gdxp->blocks as $block) {
                    $supplier_identifier = $this->supplierIdentifier($block->supplier);
                    if (isset($data[$supplier_identifier])) {
                        foreach($block->supplier->products as $product) {
                            if (isset($product->bookingInfo)) {
                                $identifier = $this->productIdentifier($product);
                                if (isset($data[$supplier_identifier]->products[$identifier])) {
                                    $data[$supplier_identifier]->products[$identifier]->quantity += $product->bookingInfo->totalQty;

                                    $data[$supplier_identifier]->products[$identifier]->subjects[] = (object) [
                                        'name' => $subject->name,
                                        'quantity' => $product->bookingInfo->totalQty,
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }

        return $data;
    }
}
