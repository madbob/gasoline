window.$ = window.jQuery = require('jquery');
require('bootstrap');
require('bootstrap-datepicker');
import "select2";

import jBob from 'jbob';

$(document).ready(function() {
    var j = new jBob();

    j.init({
        initFunction: function(container) {
            $('.date', container).datepicker({
                language: 'it',
                format: 'L',
            });

            $('[data-bs-toggle="popover"]', container).popover({
                trigger: "manual",
                html: true,
                animation:false
            })
            .on("mouseenter", function () {
                var _this = this;
                $(this).popover("show");
                $(".popover").on("mouseleave", function () {
                    $(_this).popover('hide');
                });
            }).on("mouseleave", function () {
                var _this = this;
                setTimeout(function () {
                    if (!$(".popover:hover").length) {
                        $(_this).popover("hide");
                    }
                }, 300);
            });

            $('.select2', container).each(function() {
                let url = $(this).attr('data-select-url');
                $(this).select2({
                    tags: true,
                    theme: 'bootstrap-5',
                    ajax: {
                        url: url,
                        dataType: 'json'
                    }
                });
            });

            $('.is-required', container).each(function() {
                let el = $(this);
                if (el.closest('tfoot').length == 0) {
                    el.removeClass('is-required').prop('required', true);
                }
            });

            var order_edit = $('#order_edit', container);
            if (order_edit.length) {
                order_edit.find('input[name^=pact]').change(function() {
                    var pact_id = $(this).val();
                    var status = $(this).prop('checked');
                    var suppliers_list = order_edit.find('#suppliers');
                    var subjects_list = order_edit.find('#subjects');

                    $.ajax({
                        url: '/api/pact/' + pact_id,
                        method: 'GET',
                        dataType: 'JSON',
                        success: function(data) {
                            if (status == false) {
                                data.suppliers.forEach((item, i) => {
                                    suppliers_list.find('[data-supplier-id=' + item.id + ']').remove();
                                });

                                data.subjects.forEach((item, i) => {
                                    subjects_list.find('[data-subject-id=' + item.id + ']').remove();
                                });
                            }
                            else {
                                data.suppliers.forEach((item, i) => {
                                    var c = suppliers_list.find('[data-supplier-id=' + item.id + ']');
                                    if (c.length == 0) {
                                        c = suppliers_list.find('.template').clone();
                                        c.removeClass('hidden template');
                                        c.attr('data-supplier-id', item.id);
                                        c.find('input:hidden[name^=suppliers]').val(item.id);
                                        c.find('.col-form-label').text(item.name);
                                        c.find('input:file').attr('name', 'products_file_' + item.id);
                                        suppliers_list.append(c);
                                    }
                                });

                                data.subjects.forEach((item, i) => {
                                    var c = subjects_list.find('[data-subject-id=' + item.id + ']');
                                    if (c.length == 0) {
                                        c = subjects_list.find('.template').clone();
                                        c.removeClass('hidden template');
                                        c.attr('data-subject-id', item.id);
                                        c.find('input:hidden[name^=subject]').val(item.id);
                                        c.find('.col-form-label').text(item.name);
                                        c.find('input:file').attr('name', 'booking_file_' + item.id);
                                        subjects_list.append(c);
                                    }
                                });
                            }
                        }
                    });
                });
            }
        }
    });

    $('body').on('hidden.bs.modal', '.modal', function() {
        $(this).remove();
    });

    $('.toast').each(function() {
        $(this).toast('show');
    });

    $('body').on('change', 'input[name=booking_file_multiple]', function() {
        $.ajax({
            url: '/order/mapzip/read',
            type: 'POST',
            data: new FormData($(this).closest('form').get(0)),
            processData: false,
            contentType: false,
            async: true,
            cache: false,
            enctype: 'multipart/form-data',
            dataType: 'HTML',
            success: function (data) {
                $(data).modal('show');
            }
        });
    });

    $('input[name=filter_gdxp]').change(function() {
        let filter = $(this).prop('checked');
        let column = $(this).closest('.card').find('.list-group');

        if (filter) {
            column.find('a').each(function() {
                let no_gdxp = ($(this).find('.badge-no-gdxp').length != 0);
                $(this).prop('hidden', no_gdxp);
            });
        }
        else {
            column.find('a').prop('hidden', false);
        }
    });

    $('.gdxp-serialize').submit(function(e) {
        e.preventDefault();

        var gdxp = {
            name: $(this).find('#details input[name="name"]').val(),
            vatNumber: $(this).find('#details input[name="vatNumber"]').val(),
            taxCode: $(this).find('#details input[name="taxCode"]').val(),
            address: {
                street: $(this).find('#details input[name="street"]').val(),
                locality: $(this).find('#details input[name="locality"]').val(),
                zipCode: $(this).find('#details input[name="zipCode"]').val(),
            },
            contacts: [],
            products: [],
        };

        $(this).find('#contacts tbody .item').each(function() {
            var contact = {
                type: $(this).find('select option:selected').val(),
                value: $(this).find('input[name="value"]').val(),
            };

            if (contact.value.trim() != '') {
                gdxp.contacts.push(contact);
            }
        });

        $(this).find('#products tbody .item').each(function() {
            var product = {
                name: $(this).find('input[name="name"]').val(),
                um: $(this).find('input[name="um"]').val(),
                sku: $(this).find('input[name="sku"]').val(),
                category: $(this).find('input[name="category"]').val(),
                description: $(this).find('textarea[name="description"]').val(),
                active: $(this).find('input[name="active"]').prop('checked'),
                orderInfo: {
                    packageQty: parseInt($(this).find('input[name="packageQty"]').val()),
                    umPrice: parseFloat($(this).find('input[name="umPrice"]').val()),
                    maxQty: parseFloat($(this).find('input[name="maxQty"]').val()),
                    minQty: parseFloat($(this).find('input[name="minQty"]').val()),
                    mulQty: parseFloat($(this).find('input[name="mulQty"]').val()),
                    availableQty: parseFloat($(this).find('input[name="availableQty"]').val()),
                    vatRate: parseInt($(this).find('input[name="vatRate"]').val()),
                }
            };

            if (product.name.trim() != '' && product.orderInfo.umPrice != 0) {
                gdxp.products.push(product);
            }
        });

        $.ajax({
            url: $(this).attr('action'),
            method: 'POST',
            data: {
                gdxp: JSON.stringify({supplier: gdxp}),
                _token: $('meta[name="csrf-token"]').attr('content'),
            },
            success: function() {
                location.reload();
            }
        });
    });
});
