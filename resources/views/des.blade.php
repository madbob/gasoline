@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Patti</div>

                <div class="card-body">
                    <div class="list-group">
                        @foreach($currentuser->accessiblePacts() as $pact)
                            <a href="#" class="list-group-item list-group-item-action async-modal"  data-modal-url="{{ route('pact.edit', $pact->id) }}">
                                {{ $pact->name }}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Ordini e Prenotazioni</div>

                <div class="card-body">
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action active async-modal" data-modal-url="{{ route('order.create') }}">
                            Crea Nuovo Ordine
                        </a>

                        @foreach($currentuser->accessibleOrders() as $order)
                            <a href="#" class="list-group-item list-group-item-action async-modal" data-modal-url="{{ route('order.edit', $order->id) }}">
                                {{ join(' - ', $order->pacts->reduce(function($carry, $item) { return array_merge($carry, [$item->name]); }, [])) }}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
