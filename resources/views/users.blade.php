@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Utenti</div>

                <div class="card-body">
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action active async-modal" data-modal-url="{{ route('user.create') }}">
                            Crea Nuovo Utente
                        </a>

                        @foreach($users as $user)
                            <a href="#" class="list-group-item list-group-item-action async-modal"  data-modal-url="{{ route('user.edit', $user->id) }}">
                                {{ $user->email }}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
