@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Client</div>

                <div class="card-body">
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action active async-modal" data-modal-url="{{ route('client.create') }}">
                            Crea Nuovo Client
                        </a>

                        @foreach($clients as $client)
                            <a href="#" class="list-group-item list-group-item-action async-modal"  data-modal-url="{{ route('client.edit', $client->id) }}">
                                {{ $client->name }}
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
