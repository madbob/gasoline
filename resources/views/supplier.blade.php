@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="list-group">
                        @foreach($currentuser->accessibleSuppliers() as $supplier)
                            <div class="list-group-item">
                                <x-larastrap::btngroup orientation="vertical" classes="float-end w-50">
                                    <a href="#" class="btn btn-outline-success async-modal" data-modal-url="{{ route('supplier.edit', $supplier->id) }}">Modifica Informazioni</a>
                                    <a href="{{ route('supplier.products', $supplier->id) }}" class="btn btn-outline-success">Aggiorna il Listino</a>
                                    @if($supplier->gdxpLastUpdate())
                                        <a href="{{ route('supplier.file', ['id' => $supplier->id, 'type' => 'csv']) }}" class="btn btn-outline-success">Scarica Listino CSV</a>
                                        <a href="{{ route('supplier.file', ['id' => $supplier->id, 'type' => 'gdxp']) }}" class="btn btn-outline-success">Scarica Listino GDXP</a>
                                    @endif
                                    <a href="{{ $supplier->getLandingUrl() }}" class="btn btn-outline-success" target="_blank">Visita la pagina pubblica</a>
                                </x-larastrap::btngroup>

                                {{ $supplier->name }}<br>
                                <small>Ultimo aggiornamento: {{ $supplier->gdxpLastUpdateHuman() }}</small>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
