@extends('layouts.app')

@section('content')
<div class="container-fluid">
    @if(Session::has('feedback'))
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="alert alert-info">{{ Session::get('feedback') }}</div>
            </div>
        </div>

        <hr>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-12">
            <a class="btn btn-info" href="{{ route('home.update') }}">Aggiorna riferimenti da Economia Solidale</a>
        </div>
    </div>

    <hr>

    <div class="row justify-content-center">
        @include('supplier.column')
        @include('subject.column')
        @include('pact.column')
        @include('order.column')
    </div>
</div>
@endsection
