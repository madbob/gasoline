<div class="col">
    <div class="card">
        <div class="card-header">
            <span class="float-end">
                <input type="checkbox" name="filter_gdxp"> Filtra GDXP
            </span>

            Fornitori
        </div>

        <div class="card-body">
            <div class="list-group">
                @foreach($currentuser->accessibleSuppliers() as $supplier)
                    <a href="#" class="list-group-item list-group-item-action async-modal"  data-modal-url="{{ route('supplier.edit', $supplier->id) }}">
                        {{ $supplier->name }}

                        @if(is_null($supplier->gdxpPath()))
                            <span class="badge bg-danger badge-pill badge-no-gdxp">NO GDXP</span>
                        @elseif($supplier->active == false)
                            <span class="badge bg-warning text-black badge-pill">NO ATTIVO</span>
                        @endif

                        @if($supplier->external_id == 0)
                            <span class="badge bg-info badge-pill">NO ES</span>
                        @endif
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
