@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>{{ $supplier->name }} {{ $supplier->locality ? (' - ' . $supplier->locality) : '' }}</h2>
            <h4>Ultimo aggiornamento: {{ $supplier->gdxpLastUpdateHuman() }}</h4>

            <hr>

            <p>
                Da questa pagina puoi scaricare manualmente il listino aggiornato del fornitore. Ricorda che usando <a href="{{ route('info.gdxp') }}">uno dei gestionali aderenti all'iniziativa</a> puoi ottenere i listini aggiornati in modo automatico!
            </p>
        </div>
    </div>

    <div class="row">
        @if($supplier->gdxpLastUpdate())
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">GDXP</h4>
                        <p class="card-text">Il listino in formato GDXP, importabile nei migliori gestionali per l'economia solidale. <a href="{{ route('info.gdxp') }}">Scopri di più.</a></p>
                        <a href="{{ route('supplier.file', ['id' => $supplier->id, 'type' => 'gdxp']) }}" class="btn btn-primary">Scarica File</a>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">CSV</h4>
                        <p class="card-text">Il listino in formato CSV, consultabile con qualsiasi applicazione per fogli di calcolo (Office Excel, LibreOffice Calc, Google Spreedsheet...).</p>
                        <a href="{{ route('supplier.file', ['id' => $supplier->id, 'type' => 'csv']) }}" class="btn btn-primary">Scarica File</a>
                    </div>
                </div>
            </div>
        @else
            <div class="col">
                <div class="alert alert-danger lead text-center">
                    Questo fornitore non ha ancora caricato il proprio listino!
                </div>
            </div>
        @endif
    </div>

    @if($supplier->external_id != 0)
        <br>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body text-center">
                        <p class="card-text"><a href="https://economiasolidale.net/node/{{ $supplier->external_id }}">Consulta la scheda di questo fornitore su economiasolidale.net</a></p>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

@endsection
