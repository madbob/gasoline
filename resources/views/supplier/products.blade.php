@extends('layouts.app')

@section('content')

<?php

$data = $supplier->gdxpPath();
if ($data) {
    $data = json_decode(file_get_contents($data));
}

$supplier->fromGdxp($data);

?>

<div class="container-fluid">
    @if(Session::get('saved'))
        <div class="toast-container position-absolute mt-5 p-3 top-0 end-0" style="z-index: 2000">
            <div class="toast align-items-center" data-bs-autohide="true" data-bs-delay="3000">
                <div class="d-flex">
                    <div class="toast-body">
                        Modifiche salvate.
                    </div>
                    <button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                </div>
            </div>
        </div>
    @endif

    <x-larastrap::form method="POST" :action="route('supplier.products.save', $supplier->id)" classes="gdxp-serialize" :obj="$supplier" formview="vertical">
        <div class="row">
            <div class="col-12">
                <div class="card" id="details">
                    <div class="card-header">Anagrafica</div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <x-larastrap::text name="name" label="Nome" required />
                                <x-larastrap::text name="vatNumber" label="Partita IVA" required />
                                <x-larastrap::text name="taxCode" label="Codice Fiscale" />
                            </div>

                            <div class="col">
                                <x-larastrap::field label="Indirizzo">
                                    <div class="row">
                                        <div class="col-12 mb-1">
                                            <x-larastrap::text name="street" placeholder="Indirizzo e numero civico" squeeze required />
                                        </div>
                                        <div class="col-6">
                                            <x-larastrap::text name="locality" placeholder="Città" squeeze required />
                                        </div>
                                        <div class="col-6">
                                            <x-larastrap::text name="zipCode" placeholder="CAP" squeeze required />
                                        </div>

                                        @if(empty($supplier->locality))
                                            <div class="col-12 mt-1">
                                                <small>Indicando almeno la città, sarà più semplice trovarti per i GAS vicini a te.</small>
                                            </div>
                                        @endif
                                    </div>
                                </x-larastrap::field>

                                <x-larastrap::field label="Contatti">
                                    <table class="table dynamic-table table-borderless table-sm" id="contacts">
                                        <tbody>
                                            @if($data)
                                                @foreach($supplier->contacts as $contact)
                                                    @include('supplier.partials.contact', ['contact' => $contact])
                                                @endforeach
                                            @endif

                                            <tr>
                                                <td colspan="3">
                                                    <a href="" class="btn btn-warning add-row">Aggiungi Contatto</a>
                                                    @if(empty($supplier->contacts))
                                                        <small>È consigliato aggiungere almeno un indirizzo email</small>
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot hidden>
                                            @include('supplier.partials.contact', ['contact' => (object)[]])
                                        </tfoot>
                                    </table>
                                </x-larastrap::field>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col text-end mt-2">
                                <button class="btn btn-success" type="submit">Salva</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-3 mb-2">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <x-larastrap::link label="Importa CSV" classes="float-end" triggers_modal="#importCSV" />
                        Prodotti
                    </div>

                    <div class="card-body">
                        <table class="table dynamic-table table-borderless" id="products">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Attivo</th>
                                    <th>Codice Univoco</th>
                                    <th>Unità di Misura</th>
                                    <th>Categoria</th>
                                    <th>Descrizione</th>
                                    <th>Prezzo Unitario</th>
                                    <th>Aliquota IVA</th>
                                    <th>Dimensione Confezione</th>
                                    <th>Totale Disponibile</th>
                                    <th>Elimina</th>
                                </tr>
                            </thead>
                            <tbody class="items">
                                @if($data)
                                    @foreach($supplier->products as $product)
                                        @include('supplier.partials.product', [
                                            'product' => $product,
                                            'fake' => false,
                                        ])
                                    @endforeach
                                @endif

                                <tr>
                                    <td colspan="10">
                                        <a href="" class="btn btn-warning add-row">Aggiungi Prodotto</a>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot hidden>
                                @include('supplier.partials.product', [
                                    'product' => (object)['active' => true],
                                    'fake' => true,
                                ])
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </x-larastrap::form>
</div>

<x-larastrap::modal size="lg" id="importCSV">
    <x-larastrap::form method="POST" :action="route('supplier.products.import', $supplier->id)">
        <p>
            Da qui è possibile importare un file CSV con le informazioni dei prodotti. <a href="{{ route('supplier.products.import.sample') }}">Clicca qui per scaricare il template di riferimento</a>, con le colonne attese nel file.
        </p>
        <ul>
            @foreach(\App\Helpers\GDXP::csvColumns() as $label => $desc)
                <li><strong>{{ $label }}</strong>: {{ $desc['explain'] }}</li>
            @endforeach
        </ul>
        <p>
            Se nel file CSV viene individuato un prodotto con lo stesso Codice Univoco di un prodotto già esistente, quest'ultimo viene sovrascritto coi nuovi parametri.
        </p>
        <p>
            Dopo l'importazione si raccomanda di scorrere la lista completa dei prodotti per verificare eventuali anomalie o errori.
        </p>

        <x-larastrap::file name="csv" label="File CSV" accepts=".csv" />
    </x-larastrap::form>
</x-larastrap::modal>

@endsection
