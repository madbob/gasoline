<x-larastrap::enclose :obj="$contact">
    <tr class="item">
        <td>
            <x-larastrap::select name="type" label="Tipo" squeeze :options="['emailAddress' => 'EMail', 'phoneNumber' => 'Telefono', 'mobileNumber' => 'Cellulare', 'faxNumber' => 'Fax', 'webSite' => 'Sito Web']" />
        </td>
        <td>
            <x-larastrap::text name="value" label="Valore" squeeze />
        </td>
        <td>
            <a href="#" class="btn btn-danger remove-row">Elimina</a>
        </td>
    </tr>
</x-larastrap::enclose>
