@php

if (isset($fake) == false) {
    $fake = false;
}

@endphp

<x-larastrap::enclose :obj="$product">
    <tr class="item">
        <td>
            <x-larastrap::text name="name" squeeze :required="$fake == false" :classes="$fake ? 'is-required' : ''" />
            <x-larastrap::hidden name="maxQty" />
            <x-larastrap::hidden name="minQty" />
            <x-larastrap::hidden name="mulQty" />
        </td>

        <td>
            <x-larastrap::check name="active" squeeze />
        </td>

        <td>
            <x-larastrap::text name="sku" squeeze />
        </td>

        <td>
            <x-larastrap::select name="um" classes="select2" squeeze :attributes="['data-select-url' => route('preferred.units')]" :options="$product && empty($product->um) == false ? [$product->um => $product->um] : []" />
        </td>

        <td>
            <x-larastrap::select name="category" classes="select2" squeeze :attributes="['data-select-url' => route('preferred.categories')]" :options="$product && empty($product->category) == false ? [$product->category => $product->category] : []" />
        </td>

        <td>
            <x-larastrap::textarea name="description" squeeze />
        </td>

        <td>
            <x-larastrap::number name="umPrice" squeeze :required="$fake == false" :classes="$fake ? 'is-required' : ''" textappend="€" />
        </td>

        <td>
            <x-larastrap::number name="vatRate" squeeze textappend="%" />
        </td>

        <td>
            <x-larastrap::number step="0.01" min="0" max="10000.00" name="packageQty" squeeze />
        </td>

        <td>
            <x-larastrap::number step="0.01" min="0" max="10000.00" name="availableQty" squeeze />
        </td>

        <td>
            <a href="#" class="btn btn-danger remove-row">Elimina</a>
        </td>
    </tr>
</x-larastrap::enclose>
