<x-larastrap::modal size="lg">
    <x-larastrap::form :method="$supplier ? 'PUT' : POST" :action="$supplier ? route('supplier.update', $supplier->id) : route('supplier.store')" :obj="$supplier">
        <x-larastrap::text name="name" label="Nome" required />
        <x-larastrap::text name="vat" label="Partita IVA" required />
        <x-larastrap::check name="active" label="Attivo" />

        <?php

        if ($supplier && $supplier->gdxpLastUpdate()) {
            $last_update = sprintf('Ultimo aggiornamento: %s - <a href="%s" target="_blank">Scarica il File</a>', $supplier->gdxpLastUpdateHuman(), route('api.get', $supplier->vat));
        }
        else {
            $last_update = '';
        }

        ?>
        <x-larastrap::file name="products_file" label="Carica Nuovo Listino GDXP" :help="$last_update" pophelp="È possibile generare il proprio listino GDXP autonomamente e caricarlo qui per trasmetterlo automaticamente ai gestionali." />

        @if($supplier)
            @if($currentuser->role != 'supplier')
                @if($supplier->gdxpLastUpdate())
                    <x-larastrap::check name="delete_file" label="Elimina questo listino" checked="false" />
                @endif

                <x-larastrap::field label="Listino Manuale">
                    <a href="{{ route('supplier.products', $supplier->id) }}" class="btn btn-info">Modifica manuale listino</a>
                </x-larastrap::field>

                <x-larastrap::field label="Utenti Collegati">
                    @if($supplier->users->count() == 0)
                        <input type="text" readonly class="form-control-plaintext" value="Nessun utente collegato a questo fornitore">
                    @else
                        <ul>
                            @foreach($supplier->users as $user)
                                <li>{{ $user->email }}</li>
                            @endforeach
                        </ul>
                    @endif
                </x-larastrap::field>
            @endif

            @if($currentuser->role == 'supplier')
                <hr>
                <p>
                    Includi il seguente frammento di HTML nel tuo sito per rendere accessibile direttamente il tuo listino aggiornato.
                </p>
                <p>
<pre><code>&lt;a href="{{ $supplier->getLandingUrl() }}"&gt;
    &lt;img src="{{ asset('images/hub.png') }}" style="max-width: 100%"&gt;
&lt;/a&gt;</code></pre>
                </p>
            @endif
        @endif
    </x-larastrap::form>
</x-larastrap::modal>
