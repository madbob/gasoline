<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

        <title>ERRORE</title>

        <link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}">
    </head>
    <body>
        <x-larastrap::navbar title="GASoline" classes="fixed-top" color="dark" />

        <div class="container">
            <div class="row">
                <div class="col">
                    <br><br><br><br>
                    <h1>Oops... Si è verificato un errore...</h1>
                    <br><br>
                    <p>
                        Gli errori vengono solitamente intercettati e notificati agli sviluppatori.
                    </p>
                    <p>
                        Se questo dovesse continuare a ripetersi, segnalalo all'indirizzo info@gasdotto.net avendo cura di specificare cosa stavi facendo nel momento in cui si è manifestato.
                    </p>
                    <br><br>
                    <p>
                        GASoline è in continua evoluzione... Ma di tanto in tanto ci scappa qualche svista!
                    </p>
                    <br><br>
                    <p>
                        <a class="btn btn-light btn-lg" href="{{ route('home') }}">Torna alla Home</a>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
