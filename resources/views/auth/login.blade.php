@extends('layouts.app', [
    'page_title' => 'Accedi',
    'page_claim' => 'Accedi a ' . env('APP_NAME') . ' e gestisci automaticamente i tuoi listini per i Gruppi di Acquisto Solidali',
    'page_background_image' => asset('images/login.jpg'),
])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body p-5">
                    <x-larastrap::form method="POST" :action="route('login')" :buttons="[['color' => 'success', 'label' => 'Login', 'attributes' => ['type' => 'submit']]]">
                        <x-larastrap::email name="email" label="E-Mail" />
                        <x-larastrap::password name="password" label="Password" />
                        <x-larastrap::check name="remember" label="Ricordami" checked />
                    </x-larastrap::form>

                    <div class="row mb-0">
                        <div class="col">
                            <a href="{{ route('password.request') }}">Password dimenticata?</a> | <a href="{{ route('register') }}">Non hai ancora un account? Registrati!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
