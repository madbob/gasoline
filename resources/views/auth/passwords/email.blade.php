@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Aggiorna Password</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <x-larastrap::form method="POST" :action="route('password.email')" :buttons="[['color' => 'success', 'label' => 'Invia Link per l\'Aggiornamento della Password', 'attributes' => ['type' => 'submit']]]">
                        <x-larastrap::email name="email" label="Indirizzo E-Mail" />
                    </x-larastrap::form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
