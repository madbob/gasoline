@extends('layouts.app', [
    'page_title' => 'Registrati Gratuitamente',
    'page_claim' => 'Registrati a ' . env('APP_NAME') . ' e gestisci automaticamente i tuoi listini per i Gruppi di Acquisto Solidali',
    'page_background_image' => asset('images/register.jpg'),
])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body p-5">
                    <p class="lead">
                        Registrati su {{ env('APP_NAME') }} per iniziare a veicolare automaticamente il tuo listino prodotti sempre aggiornato ai GAS, e farti trovare da nuovi gruppi di acquisto!
                    </p>

                    <hr>

                    <x-larastrap::form method="POST" :action="route('register')" :buttons="[['color' => 'success', 'label' => 'Registrati', 'attributes' => ['type' => 'submit']]]">
                        <x-larastrap::text name="name" label="Ragione Sociale" required />
                        <x-larastrap::text name="vat" label="Partita IVA" required help="Attraverso la partita IVA sarà ancora più facile trovarti, soprattutto per i GAS che già si riforniscono da te e vogliono gestire automaticamente l'aggiornamento dei listini" />
                        <x-larastrap::email name="email" label="E-Mail" required help="È consigliato usare un indirizzo email facilmente verificabile, magari quello pubblicato sul tuo sito internet o con un dominio tipo @lamiaazienda.it" />
                        <x-larastrap::password name="password" label="Password" required />
                        <x-larastrap::password name="password_confirmation" label="Conferma Password" required />

                        <?php $privacy_string = sprintf("Ho letto l'<a href=\"%s\" target=\"_blank\">informativa privacy</a>", route('info.privacy')) ?>
                        <x-larastrap::check name="privacy" :label="new \Illuminate\Support\HtmlString($privacy_string)" required />
                    </x-larastrap::form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
