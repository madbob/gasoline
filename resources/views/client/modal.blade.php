<x-larastrap::modal size="lg">
    <x-larastrap::form :method="$client ? 'PUT' : 'POST'" :action="$client ? route('client.update', $client->id) : route('client.store')" :obj="$client">
        <x-larastrap::text name="name" label="Nome" />

        @if($client)
            <x-larastrap::text name="token" label="Chiave" readonly disabled />
        @endif
    </x-larastrap::form>
</x-larastrap::modal>
