<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $page_title ?? config('app.name', 'GASoline') }}</title>
    <meta name="description" content="{{ $page_claim ?? '' }}">

    <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
    @if(isset($page_background_image))
        <div class="background" style="background-image: url('{{ $page_background_image ?? '' }}')">
        </div>
    @endif

    <div id="app">
        <?php

        $menu = [];

        $menu['Home'] = route('first');

        if (Auth::guest()) {
            $menu['Login'] = route('login');
            $menu['Registrati'] = route('register');
            $end_menu = [];
        }
        else {
            $user = Auth::user();

            if ($user->role == 'admin') {
                $menu['Utenti'] = route('user.index');
                $menu['Clients'] = route('client.index');
            }

            $end_menu['Logout'] = ['url' => route('logout'), 'attributes' => [
                'onclick' => "event.preventDefault(); document.getElementById('logout-form').submit();",
            ]];
        }

        ?>

        <x-larastrap::navbar :options="$menu" :end_options="$end_menu" classes="fixed-top" color="dark" title="GASoline" />

        @if(Auth::user())
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @endif

        <main class="py-4 mt-5">
            @yield('content')
        </main>
    </div>
</body>
</html>
