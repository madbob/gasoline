<x-larastrap::modal size="xl">
    <x-larastrap::form method="POST" :action="route('order.mapzip.save')">
        <p>
            Di seguito, l'elenco dei files letti dall'archivio compresso. Associa ogni file ad uno dei soggetti coinvolti nell'ordine per identificare tutti gli elementi.
        </p>

        <input type="hidden" name="folder_path" value="{{ $info->zip_path }}">
        <input type="hidden" name="order_id" value="{{ $order->id }}">

        <table class="table">
            <thead>
                <tr>
                    <th>Nome File</th>
                    <th>Nome GAS</th>
                    <th>Assegnazione</th>
                </tr>
            </thead>
            <tbody>
                @foreach($info->contents as $file)
                    <tr>
                        <td>
                            <input type="hidden" name="original_filename[]" value="{{ $file->file }}">
                            {{ $file->file }}
                        </td>
                        <td>
                            {{ $file->name }}
                        </td>
                        <td>
                            <select class="form-control" name="subjects[]" required>
                                <option value="0">Seleziona</option>
                                <option value="new">Crea Nuovo</option>
                                @foreach($order->subjects as $subject)
                                    <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </x-larastrap::form>
</x-larastrap::modal>
