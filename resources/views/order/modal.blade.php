<x-larastrap::modal size="xl">
    <?php $pacts = App\Pact::orderBy('created_at', 'desc')->get() ?>

    @if($pacts->isEmpty())
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    Prima di creare un nuovo Ordine devi avere almeno un Patto di riferimento.
                </div>
            </div>
        </div>
    @else
        <x-larastrap::form :method="$order ? 'PUT' : 'POST'" :action="$order ? route('order.update', $order->id) : route('order.store')" :obj="$order" enctype="multipart/form-data">
            <x-larastrap::date name="start" label="Apertura" required />
            <x-larastrap::date name="end" label="Chiusura" required />

            @if($order)
                <x-larastrap::field label="Scarica GDXP">
                    <a href="{{ route('api.order', $order->uuid) }}" class="btn btn-light" target="_blank">Listino Aggregato</a>
                </x-larastrap::field>

                <x-larastrap::field label="Aggregazione">
                    <a href="#" class="btn btn-light async-modal" data-modal-url="{{ route('order.details', $order->id) }}">Vedi Prenotazioni Aggregate</a>
                </x-larastrap::field>
            @endif

            <hr>

            <div class="form-group row" id="order_edit">
                <div class="col-sm-2">
                    <h4>
                        Patti
                    </h4>

                    @foreach($currentuser->accessiblePacts() as $pact)
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="pacts[]" value="{{ $pact->id }}" id="pact-{{ $pact->id }}" {{ $order && $order->pacts()->where('pact_id', $pact->id)->count() != 0 ? 'checked' : '' }}>
                            <label class="form-check-label" for="pact-{{ $pact->id }}">{{ $pact->name }}</label>
                        </div>
                    @endforeach
                </div>

                <div class="col-sm-5">
                    <h4>
                        Listini Fornitori
                    </h4>

                    @if(is_null($order))
                        <div class="alert alert-info mb-3">
                            Se non viene caricato qui nessun listino GDXP, viene utilizzato quello di default per il fornitore.
                        </div>
                    @endif

                    <div id="suppliers">
                        @if($order)
                            @foreach($order->suppliers as $supplier)
                                <div class="form-group row" data-supplier-id="{{ $supplier->id }}">
                                    <input type="hidden" name="suppliers[]" value="{{ $supplier->id }}">
                                    <label class="col-sm-4 col-form-label">{{ $supplier->name }}</label>
                                    <div class="col-sm-8">
                                        <input type="file" class="form-control" name="products_file_{{ $supplier->id }}">
                                        @if($supplier && $supplier->gdxpLastUpdate())
                                            <small class="form-text text-muted">
                                                Ultimo aggiornamento: {{ date('d/m/Y H:i', $supplier->gdxpLastUpdate()) }} - <a href="{{ route('api.get', $supplier->vat) }}" target="_blank">Vedi</a>
                                            </small>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif

                        <div class="form-group row hidden template" data-supplier-id="">
                            <input type="hidden" name="suppliers[]" value="">
                            <label for="name" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" name="products_file_">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5">
                    <h4>
                        Prenotazioni
                    </h4>

                    <div id="subjects">
                        @if($order)
                            <input type="hidden" name="order_id" value="{{ $order->id }}">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">File Aggregato</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" name="booking_file_multiple">
                                </div>
                            </div>

                            <hr>

                            @foreach($order->subjects as $subject)
                                <div class="form-group row" data-subject-id="{{ $subject->id }}">
                                    <input type="hidden" name="subjects[]" value="{{ $subject->id }}">
                                    <label class="col-sm-4 col-form-label">{{ $subject->name }}</label>
                                    <div class="col-sm-8">
                                        <input type="file" class="form-control" name="booking_file_{{ $subject->id }}">
                                        @if($subject->gdxpLastUpdate())
                                            <small class="form-text text-muted">
                                                Ultimo aggiornamento: {{ date('d/m/Y H:i', $subject->gdxpLastUpdate()) }}
                                            </small>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif

                        <div class="form-group row hidden template" data-subject-id="">
                            <input type="hidden" name="subjects[]" value="">
                            <label for="name" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" name="booking_file_">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </x-larastrap::form>
    @endif
</x-larastrap::modal>
