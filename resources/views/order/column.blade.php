<div class="col">
    <div class="card">
        <div class="card-header">Ordini e Prenotazioni</div>

        <div class="card-body">
            <div class="list-group">
                @can('create', App\Order::class)
                    <a href="#" class="list-group-item list-group-item-action active async-modal" data-modal-url="{{ route('order.create') }}">
                        Crea Nuovo Ordine
                    </a>
                @endif

                @foreach($currentuser->accessibleOrders() as $order)
                    <a href="#" class="list-group-item list-group-item-action async-modal" data-modal-url="{{ route('order.edit', $order->id) }}">
                        {{ join(' - ', $order->pacts->reduce(function($carry, $item) { return array_merge($carry, [$item->name]); }, [])) }}
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
