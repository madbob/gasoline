<x-larastrap::modal size="xl">
    <?php $data = $order->aggregate() ?>

    @foreach($data as $supplier)
        <h4>{{ $supplier->name }}</h4>

        @foreach($supplier->products as $product)
            <div class="row">
                <div class="col-sm-8">
                    <a class="btn btn-primary {{ $product->quantity == 0 ? 'disabled' : '' }}" data-bs-toggle="collapse" href="#product-{{ $product->uuid }}">
                        <span class="dripicons-search"></span>
                    </a>
                    {{ $product->name }}
                </div>
                <div class="col-sm-4">
                    {{ $product->quantity }}
                </div>

                <div class="col-sm-12">
                    <div class="collapse mb-3" id="product-{{ $product->uuid }}">
                        <div class="card card-body">
                            @foreach($product->subjects as $subject)
                                <div class="row">
                                    <div class="col-sm-8">
                                        {{ $subject->name }}
                                    </div>
                                    <div class="col-sm-4">
                                        {{ $subject->quantity }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endforeach

    <div class="row mt-3">
        <div class="col">
            <a href="{{ route('order.details.download', ['id' => $order->id, 'format' => 'csv']) }}" class="btn btn-success">Scarica CSV</a>
            <a href="{{ route('order.details.download', ['id' => $order->id, 'format' => 'pdf']) }}" class="btn btn-success">Scarica PDF</a>
        </div>
    </div>
</x-larastrap::modal>
