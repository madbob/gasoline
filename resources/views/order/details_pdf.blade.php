<html>
    <head>
        <style>
            table {
                border-spacing: 0;
                border-collapse: collapse;
                margin-top: 10px;
            }
        </style>
    </head>

    <body>
        @foreach($contents as $content)
            <table border="1" width="100%" cellpadding="5">
                <tr>
                    <td colspan="2" style="text-align: center">{{ $content->name }}</td>
                </tr>

                @foreach($content->products as $product)
                    <tr>
                        <td width="80%">{{ $product->name }}</td>
                        <td width="20%">{{ $product->quantity }}</td>
                    </tr>
                @endforeach
            </table>
        @endforeach
    </body>
</html>
