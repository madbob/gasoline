@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="text-center">
                    <h1 class="display-1">{{ env('APP_NAME') }}</h1>
                    <p class="display-6">L'hub dell'economia solidale, integrato dai migliori gestionali.</p>
                </div>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col">
                        <h3 class="display-4 text-center">Repository Listini<br><span class="dripicons-expand"></span></h3>
                        <p>
                            {{ env('APP_NAME') }} espone una API che permette di cercare e aggiornare automaticamente i listini dei fornitori aderenti, formalizzati in <a href="{{ route('info.gdxp') }}">formato GDXP</a> e senza dover ogni volta adeguarsi ai più disparati formati.
                        </p>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col">
                        <h3>
                            Se sei un fornitore/produttore
                        </h3>
                        <p>
                            <a href="{{ route('register') }}">Registrati</a> e <a href="{{ route('login') }}">accedi</a> con le tue credenziali per aggiornare il tuo listino, e trasmetterlo automaticamente a tutti i GAS che usano uno dei gestionali aderenti.
                        </p>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col">
                        <h3>
                            Se sei uno sviluppatore
                        </h3>

                        <p>
                            <span class="badge badge-pill bg-primary">GET</span> {{ route('api.list') }}
                        </p>
                        <p>
                            per ottenere l'elenco di tutti i fornitori nel repository in formato JSON
                        </p>
                        <p>
                            <span class="badge badge-pill bg-primary">GET</span> {{ route('api.get', 'PARTITA_IVA') }}
                        </p>
                        <p>
                            per ottenere il listino GDXP di un fornitore specifico
                            <?php $sample = App\Supplier::getRand() ?>
                            @if($sample)
                                <br>(esempio: <a href="{{ route('api.get', $sample->vat) }}">{{ route('api.get', $sample->vat) }}</a>)
                            @endif
                        </p>
                    </div>
                </div>
            </div>

            <div class="col">
                <div class="row">
                    <div class="col">
                        <h3 class="display-4 text-center">Aggregatore Ordini<br><span class="dripicons-contract"></span></h3>
                        <p>
                            {{ env('APP_NAME') }} integra un aggregatore per il <a href="{{ route('info.gdxp') }}">formato GDXP</a>, con cui condividere gli ordini con diversi GAS e riaggregarli insieme per effettuare prenotazioni collettive che coinvolgono numerosi gruppi (a prescindere dal gestionale che usano).
                        </p>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col">
                        <h3>
                            Se coordini un DES/RES
                        </h3>
                        <p>
                            <a href="{{ route('login') }}">Accedi con le tue credenziali</a> per generare i tuoi ordini collettivi e riaggregare le informazioni ottenute dai GAS che usano uno dei gestionali aderenti.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <hr class="my-4">

        <div class="row">
            <div class="col">
                <a href="https://gitlab.com/madbob/gasoline">Il codice di questa applicazione è distribuito in licenza AGPLv3</a>
            </div>
        </div>
    </div>
@endsection
