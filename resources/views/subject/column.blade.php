<div class="col">
    <div class="card">
        <div class="card-header">Soggetti</div>

        <div class="card-body">
            <div class="list-group">
                @foreach($currentuser->accessibleSubjects() as $subject)
                    <a href="#" class="list-group-item list-group-item-action async-modal"  data-modal-url="{{ route('subject.edit', $subject->id) }}">
                        {{ $subject->name }}
                    </a>

                    @if($subject->children->isEmpty() == false)
                        <div class="pl-3 list-group">
                            @foreach($subject->children as $child)
                                <a href="#" class="list-group-item list-group-item-action async-modal"  data-modal-url="{{ route('subject.edit', $child->id) }}">
                                    {{ $child->name }}
                                </a>
                            @endforeach
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
