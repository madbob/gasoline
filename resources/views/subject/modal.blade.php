<x-larastrap::modal size="lg">
    <x-larastrap::form :method="$subject ? 'PUT' : 'POST'" :action="$subject ? route('subject.update', $subject->id) : route('subject.store')" :obj="$subject">
        <x-larastrap::text name="name" label="Nome" required />
        <x-larastrap::select-model name="parent_id" label="All'interno di" :options="App\Subject::where('parent_id', 0)->orderBy('name', 'asc')->get()" extra_options="Nessuno" />

        @if($subject)
            <x-larastrap::text name="key" label="Chiave" readonly disabled />
        @endif
    </x-larastrap::form>
</x-larastrap::modal>
