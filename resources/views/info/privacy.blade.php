@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>Informativa Privacy</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="card-text">
                        <p>
                            I dati privati degli utenti (indirizzo email e password) non sono condivisi con nessuno al di fuori degli sviluppatori di GASoline, e vengono utilizzati solo per i fini della piattaforma stessa (ovvero: pubblicare e tenere aggiornati i listini).
                        </p>
                        <p>
                            Viceversa, i dati dei listini caricati (inclusi i contatti) sono per definizione pubblici e potenzialmente accessibili da chiunque.
                        </p>
                        <p>
                            I listini che non vengono aggiornati per 365 giorni vengono automaticamente rimossi dall'indice. Per far rimuovere tutti i propri dati - l'account ed il listino - invia una mail a info@gasdotto.net
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
