@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>GDXP e Gestionali</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <p class="card-text">
                        GDXP è il formato di interoperabilità tra i gestionali dell'economia solidale. Permette di rappresentare listini, ordini e prenotazioni in modo univoco per tutte le applicazioni che lo adottano, semplificando ed accelerando la condivisione di informazioni anche tra piattaforme diverse.
                    </p>
                    <p class="card-text">
                        Qui si trova la <a href="https://github.com/madbob/gdxp">documentazione tecnica</a>, rivolta a chi vuole implementarlo nella sua propria applicazione per fornire ai propri utenti tutti i vantaggi dello scambio dati, ed è possibile avanzare segnalazioni e suggerimenti per la sua evoluzione. Si raccomanda di usare la versione v1 del formato, serializzata in JSON.
                    </p>
                    <p class="card-text">
                        Tra le applicazioni che supportano il formato GDXP:
                    </p>
                    <ul>
                        <li><a href="https://www.gasdotto.net/">GASdotto</a></li>
                        <li><a href="https://www.portalgas.it/">PortalGAS</a></li>
                        <li><a href="https://retegas.altervista.org/">ReteDES</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
