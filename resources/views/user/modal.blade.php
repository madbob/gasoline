<x-larastrap::modal size="lg">
    <x-larastrap::form :method="$user ? 'PUT' : 'POST'" :action="$user ? route('user.update', $user->id) : route('user.store')" :obj="$user">
        <x-larastrap::email name="email" label="E-Mail" />
        <x-larastrap::password name="password" label="Password" :placeholder="$user ? 'Lascia in bianco per non modificare' : ''" :required="$user == null" />
        <x-larastrap::radios name="role" label="Ruolo" :options="['admin' => 'Amministratore', 'supplier' => 'Fornitore', 'des' => 'DES/RES']" />

        <x-larastrap::field label="Fornitori (gli amministratori hanno sempre accesso a tutti i fornitori)">
            @foreach(App\Supplier::orderBy('name', 'asc')->get() as $supplier)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="suppliers[]" value="{{ $supplier->id }}" id="supplier-{{ $supplier->id }}" {{ $user && $user->suppliers()->where('supplier_id', $supplier->id)->count() != 0 ? 'checked' : '' }}>
                    <label class="form-check-label" for="supplier-{{ $supplier->id }}">{{ $supplier->name }}</label>
                </div>
            @endforeach
        </x-larastrap::field>

        <x-larastrap::field label="Patti (gli amministratori hanno sempre accesso a tutti i patti)">
            @foreach(App\Pact::orderBy('name', 'asc')->get() as $pact)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="pacts[]" value="{{ $pact->id }}" id="pact-{{ $pact->id }}">
                    <label class="form-check-label" for="pact-{{ $pact->id }}">{{ $pact->name }}</label>
                </div>
            @endforeach
        </x-larastrap::field>
    </x-larastrap::form>
</x-larastrap::modal>
