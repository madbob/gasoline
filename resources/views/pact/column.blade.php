<div class="col">
    <div class="card">
        <div class="card-header">Patti</div>

        <div class="card-body">
            <div class="list-group">
                @foreach($currentuser->accessiblePacts() as $pact)
                    <a href="#" class="list-group-item list-group-item-action async-modal"  data-modal-url="{{ route('pact.edit', $pact->id) }}">
                        {{ $pact->name }}
                    </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
