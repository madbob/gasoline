<x-larastrap::modal size="lg">
    <x-larastrap::form :method="$pact ? 'PUT' : 'POST'" :action="$pact ? route('pact.update', $pact->id) : route('pact.store')" :obj="$pact">
        <x-larastrap::text name="name" label="Nome" required />

        <hr>

        <div class="form-group row">
            <div class="col-sm-6">
                <h4>
                    Fornitori
                </h4>

                @foreach($currentuser->accessibleSuppliers() as $supplier)
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="suppliers[]" value="{{ $supplier->id }}" id="supplier-{{ $supplier->id }}" {{ $pact && $pact->suppliers()->where('supplier_id', $supplier->id)->count() != 0 ? 'checked' : '' }}>
                        <label class="form-check-label" for="supplier-{{ $supplier->id }}">{{ $supplier->name }}</label>
                    </div>
                @endforeach
            </div>

            <div class="col-sm-6">
                <h4>
                    Soggetti
                </h4>

                <?php

                function recursiveSubjects($subjects, $deep, $pact, $checked)
                {
                    $offset = join('', array_fill(0, $deep, '&nbsp;'));

                    foreach($subjects as $s) {
                        $s_checked = ($checked || ($pact && $pact->subjects()->where('subject_id', $s->id)->count() != 0));

                        echo '<div class="form-check">
                            <input class="form-check-input" type="checkbox" name="subjects[]" value="' . $s->id . '" id="subject-' . $s->id . '" ' . ($s_checked ? 'checked' : '') . '>
                            <label class="form-check-label" for="subject-' . $s->id . '">' . $offset . $s->name . '</label>
                        </div>';

                        recursiveSubjects($s->children, $deep + 3, $pact, $s_checked);
                    }
                }

                recursiveSubjects(App\Subject::where('parent_id', 0)->orderBy('name', 'asc')->get(), 0, $pact, false);

                ?>
            </div>
        </div>
    </x-larastrap::form>
</x-larastrap::modal>
