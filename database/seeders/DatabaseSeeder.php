<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $u = new User();
        $u->email = 'admin@admin.it';
        $u->name = 'Amministratore';
        $u->password = Hash::make('cippalippa');
        $u->email_verified_at = time();
        $u->role = 'admin';
        $u->save();
    }
}
