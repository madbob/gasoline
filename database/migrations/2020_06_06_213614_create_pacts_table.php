<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->string('name')->default('');
            $table->string('uuid');
        });

        Schema::create('pact_supplier', function (Blueprint $table) {
            $table->unsignedBigInteger('pact_id');
            $table->unsignedBigInteger('supplier_id');

            $table->foreign('pact_id')->references('id')->on('pacts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('pact_subject', function (Blueprint $table) {
            $table->unsignedBigInteger('pact_id');
            $table->unsignedBigInteger('subject_id');

            $table->foreign('pact_id')->references('id')->on('pacts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade')->onUpdate('cascade');
        });

        Schema::create('pact_user', function (Blueprint $table) {
            $table->unsignedBigInteger('pact_id');
            $table->unsignedBigInteger('user_id');

            $table->foreign('pact_id')->references('id')->on('pacts')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pact_supplier');
        Schema::dropIfExists('pact_subject');
        Schema::dropIfExists('pact_user');
        Schema::dropIfExists('pacts');
    }
}
