<?php

return [
    /*
        All the values in "commons" and the different arrays into "elements" may be defined everywhere.
        Those specific for an "elements" tag type will take precedence on those defined in "commons".
    */

    'commons' => [
        'label_width' => ["xs" => 12, "md" => 4],
        'input_width' => ["xs" => 12, "md" => 8],
    ],

    'elements' => [
        'navbar' => [
            'color' => 'light',
        ],

        'form' => [
            'formview' => 'horizontal',
            'method' => 'POST',
            'enctype' => '',

            /*
                This is required to trigger Bootstrap validation
                Cfr. https://getbootstrap.com/docs/5.0/forms/validation/
            */
            'novalidate' => true,

            'buttons' => [
                [
                    'color' => 'success',
                    'label' => 'Salva',
                    'attributes' => [
                        'type' => 'submit',
                    ]
                ]
            ],
        ],

        'modal' => [
            'buttons' => [
                [
                    'color' => 'secondary',
                    'label' => 'Chiudi',
                    'attributes' => ['data-bs-dismiss' => 'modal'],
                ]
            ]
        ],

        'number' => [
            'step' => 1,
            'min' => PHP_INT_MIN,
            'max' => PHP_INT_MAX,
        ],

        'range' => [
            'step' => 1,
            'min' => PHP_INT_MIN,
            'max' => PHP_INT_MAX,
        ],

        'radios' => [
            'color' => 'outline-primary',
        ],

        'checks' => [
            'color' => 'outline-primary',
        ],

        'tabs' => [
            'tabview' => 'tabs',
        ],

        'tabpane' => [
            'classes' => ['p-3']
        ]
    ],
];
