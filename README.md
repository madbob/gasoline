## GASoline

GASoline è l'hub dell'economia solidale. Permette di caricare listini in [formato GDXP](https://github.com/madbob/gdxp) dei fornitori, anche in diverse versioni a seconda di convenzioni e patti esistenti, ed esporli tramite una semplice API fruibile dai gestionali dei GAS.

## API

`/api/list` - restituisce l'elenco completo dei fornitori per i quali è stato caricato un listino, indicando nome e partita IVA.

`/api/get/$partitaiva` - restituisce il listino GDXP del fornitore di cui è esplicitata la partita IVA.

## Licenza

GASoline è distribuito in licenza AGPLv3.
